
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// Alamofire
#define COCOAPODS_POD_AVAILABLE_Alamofire
#define COCOAPODS_VERSION_MAJOR_Alamofire 1
#define COCOAPODS_VERSION_MINOR_Alamofire 3
#define COCOAPODS_VERSION_PATCH_Alamofire 1

// Bolts
#define COCOAPODS_POD_AVAILABLE_Bolts
#define COCOAPODS_VERSION_MAJOR_Bolts 1
#define COCOAPODS_VERSION_MINOR_Bolts 2
#define COCOAPODS_VERSION_PATCH_Bolts 0

// Bolts/AppLinks
#define COCOAPODS_POD_AVAILABLE_Bolts_AppLinks
#define COCOAPODS_VERSION_MAJOR_Bolts_AppLinks 1
#define COCOAPODS_VERSION_MINOR_Bolts_AppLinks 2
#define COCOAPODS_VERSION_PATCH_Bolts_AppLinks 0

// Bolts/Tasks
#define COCOAPODS_POD_AVAILABLE_Bolts_Tasks
#define COCOAPODS_VERSION_MAJOR_Bolts_Tasks 1
#define COCOAPODS_VERSION_MINOR_Bolts_Tasks 2
#define COCOAPODS_VERSION_PATCH_Bolts_Tasks 0

// FBSDKCoreKit
#define COCOAPODS_POD_AVAILABLE_FBSDKCoreKit
#define COCOAPODS_VERSION_MAJOR_FBSDKCoreKit 4
#define COCOAPODS_VERSION_MINOR_FBSDKCoreKit 6
#define COCOAPODS_VERSION_PATCH_FBSDKCoreKit 0

// FBSDKCoreKit/arc
#define COCOAPODS_POD_AVAILABLE_FBSDKCoreKit_arc
#define COCOAPODS_VERSION_MAJOR_FBSDKCoreKit_arc 4
#define COCOAPODS_VERSION_MINOR_FBSDKCoreKit_arc 6
#define COCOAPODS_VERSION_PATCH_FBSDKCoreKit_arc 0

// FBSDKCoreKit/no-arc
#define COCOAPODS_POD_AVAILABLE_FBSDKCoreKit_no_arc
#define COCOAPODS_VERSION_MAJOR_FBSDKCoreKit_no_arc 4
#define COCOAPODS_VERSION_MINOR_FBSDKCoreKit_no_arc 6
#define COCOAPODS_VERSION_PATCH_FBSDKCoreKit_no_arc 0

// FBSDKLoginKit
#define COCOAPODS_POD_AVAILABLE_FBSDKLoginKit
#define COCOAPODS_VERSION_MAJOR_FBSDKLoginKit 4
#define COCOAPODS_VERSION_MINOR_FBSDKLoginKit 6
#define COCOAPODS_VERSION_PATCH_FBSDKLoginKit 0

// KGFloatingDrawer
#define COCOAPODS_POD_AVAILABLE_KGFloatingDrawer
#define COCOAPODS_VERSION_MAJOR_KGFloatingDrawer 0
#define COCOAPODS_VERSION_MINOR_KGFloatingDrawer 1
#define COCOAPODS_VERSION_PATCH_KGFloatingDrawer 3

// UIColor-Hex-Swift
#define COCOAPODS_POD_AVAILABLE_UIColor_Hex_Swift
#define COCOAPODS_VERSION_MAJOR_UIColor_Hex_Swift 0
#define COCOAPODS_VERSION_MINOR_UIColor_Hex_Swift 1
#define COCOAPODS_VERSION_PATCH_UIColor_Hex_Swift 0

