//
//  CGFloat_SE.swift
//  Swift Exercise
//
//  Created by Artur Jaworski on 18.08.2015.
//  Copyright (c) 2015 brckt. All rights reserved.
//

import Foundation
import UIKit

extension CGFloat {
    static func hairlineSize() -> CGFloat {
        return (1.0 / UIScreen.mainScreen().scale)
    }
}