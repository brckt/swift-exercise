//
//  LoginViewController.swift
//  Swift Exercise
//
//  Created by Artur Jaworski on 18.08.2015.
//  Copyright (c) 2015 brckt. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import KGFloatingDrawer

class LoginViewController: UIViewController {
    @IBOutlet weak var loginButton: SERoundedButton!
    var isVisible: Bool = false
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.onInit()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.onInit()
    }
    
    private func onInit() -> Void {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "onNotification:", name: Session.SessionStateDidChange, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.colorWithSEColor(SEColor.GrayBackground)
        self.title = NSLocalizedString("Log in", comment: "")
        
        self.loginButton.addTarget(self, action: "onLoginButton:", forControlEvents: UIControlEvents.TouchUpInside)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isVisible = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.isVisible = false
    }
    
    func onLoginButton(button: UIButton) -> Void {
        Session.sharedInstance.performLoginWithFacebook()
    }
    
    func onNotification(notification: NSNotification) -> Void {
        if(notification.name == Session.SessionStateDidChange) {
            if(self.isVisible) {
                if(Session.sharedInstance.state == SessionState.LoggingIn) {
                    // show progressbar
                }
                else {
                    // hide progressbar
                    
                    if(Session.sharedInstance.state == SessionState.LoggedIn) {
                        self.showAfterLoginViewController()
                    }
                    else if(Session.sharedInstance.state == SessionState.LoggedOut) {
                        // show alert
                    }
                }
            }
            else {
                if(Session.sharedInstance.state == SessionState.LoggedOut) {
                    self.dismissViewControllerAnimated(true, completion:nil)
                }
                else {
                    self.showAfterLoginViewController()
                }
            }
        }
    }
    
    private func showAfterLoginViewController() -> Void {
        let navigationController = BasicNavigationController()
        let viewController = MyAccountViewController(nibName: "MyAccountViewController", bundle: nil)
        navigationController.pushViewController(viewController, animated: false)
        
        let slideMenuViewController = SlideMenuViewController(nibName: "SlideMenuViewController", bundle: nil)
        
        let drawerViewController = KGDrawerViewController()
        
        drawerViewController.leftViewController = slideMenuViewController
        drawerViewController.centerViewController = navigationController
        drawerViewController.backgroundImage = UIImage(named: "LoginBackground")
        drawerViewController.leftDrawerWidth = 230
        
        drawerViewController.modalPresentationStyle = UIModalPresentationStyle.FullScreen
        navigationController.drawerViewController = drawerViewController
        slideMenuViewController.drawerViewController = drawerViewController
        self.presentViewController(navigationController.drawerViewController!, animated: true, completion: nil)
    }
}
