//
//  SlideMenuViewController.swift
//  Swift Exercise
//
//  Created by Artur Jaworski on 18.08.2015.
//  Copyright (c) 2015 brckt. All rights reserved.
//

import UIKit
import KGFloatingDrawer

class SlideMenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    @IBOutlet weak var tableView: UITableView!
    weak var drawerViewController: KGDrawerViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = UIColor.clearColor()
        self.tableView.backgroundColor = UIColor.clearColor()
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        self.tableView.layoutMargins = UIEdgeInsetsMake(0, 45, 0, 0)
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var identifier = "SeparatorCell"
        if(indexPath.section == 1) {
            identifier = "ClickableCell"
        }
        
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(identifier) as? UITableViewCell
        if(cell == nil) {
            cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: identifier)
        }
        
        cell?.selectionStyle = UITableViewCellSelectionStyle.None
        cell?.backgroundColor = UIColor.clearColor()
        cell?.textLabel?.text = ""
        cell?.textLabel?.font = UIFont(name: UIFont.SEFontNameWithStyle(SEFontStyle.Semibold), size: UIFont.SEFontSizeWithSize(SEFontSize.SlideMenu))
        cell?.textLabel?.textColor = UIColor.whiteColor()
        cell?.imageView?.image = nil
        
        if(indexPath.section != 1) {
            return cell!
        }
        
        if(indexPath.row == 0) {
            cell?.imageView?.image = UIImage(named: "my-account")
            cell?.textLabel?.text = NSLocalizedString("MY ACCOUNT", comment: "")
        }
        else if(indexPath.row == 1) {
            cell?.imageView?.image = UIImage(named: "favorites")
            cell?.textLabel?.text = NSLocalizedString("FAVORITES", comment: "")
        }
        else if(indexPath.row == 2) {
            cell?.imageView?.image = UIImage(named: "about-us")
            cell?.textLabel?.text = NSLocalizedString("ABOUT US", comment: "")
        }
        else if(indexPath.row == 3) {
            cell?.imageView?.image = UIImage(named: "logout")
            cell?.textLabel?.text = NSLocalizedString("LOGOUT", comment: "")
        }
        
        return cell!
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 1) {
            return 4
        }
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == 1) {
            return 50
        }
        
        return (self.view.bounds.size.height-self.tableViewHeight())/2
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(indexPath.section != 1) {
            return
        }
        
        var targetViewController: UIViewController? = nil
        let navigationController: UINavigationController = self.drawerViewController?.centerViewController! as! UINavigationController
        
        if(indexPath.row == 0) {
            if(navigationController.viewControllers.count > 1) {
                navigationController.popToRootViewControllerAnimated(true)
            }
            self.drawerViewController?.closeDrawer(KGDrawerSide.Left, animated: true, complete: { (finished) -> Void in })
        }
        else if(indexPath.row == 1) {
            
        }
        else if(indexPath.row == 2) {
            
        }
        else if(indexPath.row == 3) {
            Session.sharedInstance.logout()
        }
        
        if(targetViewController != nil) {
            let rootViewController = navigationController.viewControllers.first as! UIViewController
            navigationController.setViewControllers([rootViewController, targetViewController!], animated: true)
            self.drawerViewController?.closeDrawer(KGDrawerSide.Left, animated: true, complete: { (finished) -> Void in })
        }
        
        self.tableView.deselectRowAtIndexPath(indexPath, animated: false)
    }
    
    private func tableViewHeight() -> CGFloat {
        var height: CGFloat = 0
        
        for var row = 0; row < self.tableView(self.tableView, numberOfRowsInSection: 1); row++ {
            height += self.tableView(self.tableView, heightForRowAtIndexPath: NSIndexPath(forRow: row, inSection: 1))
        }
        
        return height
    }
}
