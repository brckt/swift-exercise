//
//  UIFont_SE.swift
//  Swift Exercise
//
//  Created by Artur Jaworski on 18.08.2015.
//  Copyright (c) 2015 brckt. All rights reserved.
//

import Foundation
import UIKit

enum SEFontStyle {
    case Regular, Semibold
}

enum SEFontSize {
    case NavigationBar, SlideMenu
}

extension UIFont {
    class func SEFontSizeWithSize(size: SEFontSize) -> CGFloat {
        switch(size) {
            case SEFontSize.NavigationBar:
                return 17.0
            case SEFontSize.SlideMenu:
                return 14.0
            default:
                return 0
        }
    }
    
    class func SEFontNameWithStyle(style: SEFontStyle) -> String {
        var fontName = "OpenSans";
        
        var suffix: String = ""
        switch(style) {
            case SEFontStyle.Semibold:
                suffix = "Semibold"
                break;
            default:
                suffix = ""
                break;
        }
        
        if(suffix != "") {
            fontName += "-"
            fontName += suffix
        }
        
        return fontName;
    }
}