//
//  Session.swift
//  Swift Exercise
//
//  Created by Artur Jaworski on 18.08.2015.
//  Copyright (c) 2015 brckt. All rights reserved.
//

import Foundation

import FBSDKCoreKit
import FBSDKLoginKit

enum SessionState {
    case Initial, LoggedOut, LoggingIn, LoggedIn
}

class Session {
    static let sharedInstance = Session()
    static let SessionStateDidChange = "SessionStateDidChange"
    
    var _state: SessionState = SessionState.Initial
    var state: SessionState {
        get {
            return _state
        }
        set(newValue) {
            if(self._state == newValue) {
                return
            }
            
            if(!self.shouldChangeState(newValue)) {
                NSLog("can't change state from %u to %u", self._state.hashValue, newValue.hashValue)
                return
            }
            
            _state = newValue
            NSLog("new state: %u", self._state.hashValue);
            NSNotificationCenter.defaultCenter().postNotificationName(Session.SessionStateDidChange, object: nil)
        }
    }
    
    private func shouldChangeState(newValue: SessionState) -> Bool {
        switch(newValue) {
            case .Initial:
                return false
            case .LoggedOut:
                return true
            case .LoggingIn:
                if(self.state.hashValue <= SessionState.LoggedOut.hashValue) {
                    return true
                }
            case .LoggedIn:
                if(self.state == SessionState.LoggingIn) {
                    return true;
                }
        }
        
        return false
    }
    
    func performLoginWithFacebook() -> Void {
        self.state = SessionState.LoggingIn
        
        let accessToken: FBSDKAccessToken? = FBSDKAccessToken.currentAccessToken()
        if(accessToken != nil) {
            self.state = SessionState.LoggedIn
            return
        }
        
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logInWithReadPermissions([ "public_profile", "email" ], handler: { (result: FBSDKLoginManagerLoginResult!, error: NSError!) -> Void in
            if(error == nil) {
                if(result.isCancelled) {
                    self.state = SessionState.LoggedOut
                }
                else {
                    self.state = SessionState.LoggedIn
                }
            }
            else {
                self.state = SessionState.LoggedOut
            }
        })
    }
    
    func logout() -> Void {
        FBSDKAccessToken.setCurrentAccessToken(nil)
        self.state = SessionState.LoggedOut
    }
}