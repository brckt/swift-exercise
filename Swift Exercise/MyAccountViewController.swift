//
//  MyAccountViewController.swift
//  Swift Exercise
//
//  Created by Artur Jaworski on 18.08.2015.
//  Copyright (c) 2015 brckt. All rights reserved.
//

import UIKit
import KGFloatingDrawer

class MyAccountViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "hamburger"), style: UIBarButtonItemStyle.Plain, target: self, action: "onHamburger")        
    }
    
    func onHamburger() {
        let navigationController: BasicNavigationController = self.navigationController as! BasicNavigationController
        navigationController.drawerViewController?.toggleDrawer(KGDrawerSide.Left, animated: true, complete: { (finished: Bool) -> Void in })
    }
}
