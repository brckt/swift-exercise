//
//  UIColor_SE.swift
//  Swift Exercise
//
//  Created by Artur Jaworski on 18.08.2015.
//  Copyright (c) 2015 brckt. All rights reserved.
//

import Foundation
import UIKit
import UIColor_Hex_Swift

enum SEColor {
    case GrayBackground, GrayHairline
}

extension UIColor {
    class func colorWithSEColor(color: SEColor) -> UIColor? {
        switch(color) {
            case SEColor.GrayBackground:
                return UIColor.colorWithCSS("#262626")
            case SEColor.GrayHairline:
                return UIColor.colorWithCSS("#aaaaaa")
            default:
                return nil
        }
    }
}