//
//  LoginNavigationController.swift
//  Swift Exercise
//
//  Created by Artur Jaworski on 18.08.2015.
//  Copyright (c) 2015 brckt. All rights reserved.
//

import UIKit

class LoginNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.clearColor()
        
        self.navigationBar.translucent = true
        self.navigationBar.tintColor = UIColor.clearColor()
        self.navigationBar.backgroundColor = UIColor.clearColor()
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationBar.shadowImage = UIImage.imageWithColor(UIColor.colorWithSEColor(SEColor.GrayHairline)!, size: CGSizeMake(CGFloat.hairlineSize(), CGFloat.hairlineSize()))
        self.navigationBar.titleTextAttributes = [ NSForegroundColorAttributeName: UIColor.whiteColor(), NSFontAttributeName : UIFont(name: UIFont.SEFontNameWithStyle(SEFontStyle.Regular), size: UIFont.SEFontSizeWithSize(SEFontSize.NavigationBar))! ]
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
}
